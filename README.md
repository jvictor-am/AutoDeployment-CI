# Integração GitLab Runner - Gitlab CI - Docker Containers - Portainer

Para criar um GitLab Runner local através do Docker, no seu projeto do GitLab vá até Settings -> CI/CD -> Runners -> New Project Runner.

#### Platform
 - Linux
#### Tags
- Run untagged jobs

#### Opcional
- Lock to current projects 

Apos criar o Token do Runner guarde-o.


## Criar o Runner do Docker

Primeiro precisamos criar um volume para o Runner:
```bash
docker volume create gitlab-runner-config
```
Para criar o container, use:
```bash
docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest
```

Para autenticar e definir o runner, use:
```bash
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```
Aqui será solicitada a URL do GitLab:
```bash
https://gitlab.com
or
https://gitlab.localdomain.com
```
Token:
```bash
Insira o token gerado na primeira etapa.
```

Executor:
```bash
docker
```

Imagem Docker para o runner:
```bash
alpine:latest
```

## Finalizando o Runner
Após essa configuração, vamos entrar no container para mapear o volume do certificado TLS e ativar o modo de privilégio para execução do Docker-in-Docker.

Entrando no bash do container:   
```python
docker exec -it gitlab-runner bash
```

Capturando as configurações:
```
cat /etc/gitlab-runner/config.toml
``` 

Vão aparecer configurações como estas:
```
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "PC-Home"
  url = "https://gitlab.com"
  id = ID
  token = "TT"
  token_obtained_at = 
  token_expires_at = 
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    network_mtu = 0
``` 

Modifique os seguintes parâmetros:
```
volumes = ["/cache"]
```
para
```
volumes = ["/certs/client", "/cache"]
```
e
```
privileged = false
```
para
```
privileged = true
```

Assim estará a sua configuração:
```
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "PC-Home"
  url = "https://gitlab.com/"
  id = ID
  token = "TT"
  token_obtained_at = 
  token_expires_at = 
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/certs/client", "/cache"]
    shm_size = 0
    network_mtu = 0
```

Dessa forma, basta usar esse runner para rodar jobs como teste e build, fazendo a autenticação das credenciais na área de variáveis nas configurações de CI/CD.


## Deploy automatizado

Usando o Portainer, podemos criar uma Stack e usar seu Webhook pelo GitLab para disparar um repull.

Primeiro, entre no Portainer e configure uma nova Stack para usar o repositório do GitLab, passando login, senha e URL do repositório.

No Mechanism, selecione Webhook e copie o URL do webhook.

No seu repositório do GitLab, vá em settings -> Webhooks, cole a sua URL e selecione Pipeline events.

Quando houver um evento em sua pipeline, o Portainer irá tentar atualizar sua Stack.

## Data
04/03/2024